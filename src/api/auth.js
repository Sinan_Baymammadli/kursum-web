import axios from "axios";

export default {
  login: data => axios.post("/api/login", data).then(res => res.data),
  register: data => axios.post("/api/register", data).then(res => res.data),
};
