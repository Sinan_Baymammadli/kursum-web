const capitalizeFirstLetter = string => {
  const lowercaseString = string.toLowerCase();
  return lowercaseString.charAt(0).toUpperCase() + string.substring(1);
};

export default capitalizeFirstLetter;
