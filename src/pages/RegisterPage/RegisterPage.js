import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { register } from "../../actions/auth";
import RegisterForm from "../../components/authForms/RegisterForm/RegisterForm";

class RegisterPage extends Component {
  submit = data =>
    this.props.register(data).then(() => this.props.history.push("/profile"));

  render() {
    return (
      <div>
        <h1>RegisterPage</h1>
        <RegisterForm submit={this.submit} />
      </div>
    );
  }
}

RegisterPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  register: PropTypes.func.isRequired,
};

export default connect(null, { register })(RegisterPage);
