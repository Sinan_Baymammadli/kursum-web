import { LOGGED_IN, LOGGED_OUT } from "../constants/actionTypes";

export default function auth(state = {}, action) {
  switch (action.type) {
    case LOGGED_IN:
      return action.payload;
    case LOGGED_OUT:
      return null;
    default:
      return state;
  }
}
