import { LOGGED_IN, LOGGED_OUT } from "../constants/actionTypes";
import auth from "../api/auth";

export const loggedIn = user => ({
  type: LOGGED_IN,
  payload: user,
});

export const loggedOut = () => ({
  type: LOGGED_OUT,
});

export const register = data => dispatch =>
  auth.register(data).then(user => dispatch(loggedIn(user)));

export const login = data => dispatch =>
  auth.login(data).then(user => dispatch(loggedIn(user)));

export const logout = () => dispatch => dispatch(loggedOut());
