import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";

import s from "./Input.css";

const cx = classNames.bind(s);

const Input = ({ type, name, label, value, onChange }) => (
  <div className={cx("field")}>
    <label htmlFor={name} className={cx("label")}>
      {label}
    </label>
    <input
      type={type}
      placeholder={label}
      id={name}
      name={name}
      value={value}
      onChange={onChange}
      className={cx("input")}
    />
  </div>
);

Input.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Input;
