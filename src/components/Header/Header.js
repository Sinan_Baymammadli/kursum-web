import React from "react";
import { Link } from "react-router-dom";
import classNames from "classnames/bind";

import s from "./Header.css";

const cx = classNames.bind(s);

const Header = () => (
  <header className={cx("header")}>
    <div className={cx("container")}>
      <Link to="/" className={cx("link")}>
        Home
      </Link>
      <Link to="/login" className={cx("link")}>
        Login
      </Link>
      <Link to="/register" className={cx("link")}>
        Register
      </Link>
      <Link to="/profile" className={cx("link")}>
        Profile
      </Link>
    </div>
  </header>
);

export default Header;
