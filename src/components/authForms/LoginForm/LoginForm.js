import React, { Component } from "react";
import PropTypes from "prop-types";

import Input from "../../Input/Input";
import Button from "../../Button/Button";

class LoginForm extends Component {
  state = {
    data: {
      email: "",
      password: "",
    },
    loading: false,
  };

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value },
    });

  onSubmit = e => {
    e.preventDefault();
    this.props.submit(this.state.data);
  };

  render() {
    const { email, password } = this.state.data;

    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <Input
            type="email"
            name="email"
            label="Email"
            value={email}
            onChange={this.onChange}
          />
          <Input
            type="password"
            name="password"
            label="Password"
            value={password}
            onChange={this.onChange}
          />
          <Button text="Login" />
        </form>
      </div>
    );
  }
}

LoginForm.propTypes = {
  submit: PropTypes.func.isRequired,
};

export default LoginForm;
