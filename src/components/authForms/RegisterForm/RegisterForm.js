import React, { Component } from "react";
import PropTypes from "prop-types";

import Input from "../../Input/Input";
import Button from "../../Button/Button";

class RegisterForm extends Component {
  state = {
    data: {
      name: "",
      email: "",
      password: "",
      passwordConfirmation: "",
    },
    loading: false,
  };

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value },
    });

  onSubmit = e => {
    e.preventDefault();
    this.props.submit(this.state.data);
  };

  render() {
    const { name, email, password, passwordConfirmation } = this.state.data;

    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <Input
            type="text"
            name="name"
            label="Name"
            value={name}
            onChange={this.onChange}
          />
          <Input
            type="email"
            name="email"
            label="Email"
            value={email}
            onChange={this.onChange}
          />
          <Input
            type="password"
            name="password"
            label="Password"
            value={password}
            onChange={this.onChange}
          />
          <Input
            type="password"
            name="passwordConfirmation"
            label="Confirm password"
            value={passwordConfirmation}
            onChange={this.onChange}
          />
          <Button text="Register" />
        </form>
      </div>
    );
  }
}

RegisterForm.propTypes = {
  submit: PropTypes.func.isRequired,
};

export default RegisterForm;
