import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";

import s from "./Button.css";

const cx = classNames.bind(s);

const Button = ({ text }) => <button className={cx("button")}>{text}</button>;

Button.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Button;
